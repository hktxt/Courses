# Courses
Quiz & Assignment & Materials of courses I learned. Some of them are available on Coursera(https://www.coursera.org/) and the others are university courses which videoed and available online. I found these good courses quite uesfull and have already learned some of them and some are in progressing. I list them and preserve the materials and wish that would help you guys.

### 1. NTU_Probability-I-II(台湾大学，叶丙成， 顽想学概率一，二)
 * [概率一](https://www.coursera.org/learn/prob1) :heavy_check_mark: 
 * [概率二](https://www.coursera.org/learn/prob2) :heavy_check_mark:

### 2. MachineLearing-Hsuan-Tien-Lin（机器学习基石&技法，台湾大学，林轩田）
 * 机器学习基石：[上](https://www.coursera.org/learn/ntumlone-mathematicalfoundations) , [下]( https://www.coursera.org/learn/ntumlone-algorithmicfoundations) :heavy_check_mark:
 * 机器学习技法：

### 3. Probabilistic Graphical Models, Stanford University
 * [Probabilistic Graphical Models 1: Representation](https://www.coursera.org/learn/probabilistic-graphical-models/home/welcome) :heavy_check_mark:
 * Probabilistic Graphical Models 2: 
 * Probabilistic Graphical Models 3: 
 
### 4. CMU-11-785-Fall-2018, 11-485/785 Introduction to Deep Learning
 * [Introduction to Deep Learning](http://deeplearning.cs.cmu.edu/)
 
### 5. CMU_PGM_Eric Xing, Probabilistic Graphical Models
 * [Probabilistic Graphical Models](http://www.cs.cmu.edu/~epxing/Class/10708-14/lecture.html)
 
 ### 6. EE364
 * [EE364a: Convex Optimization I](http://web.stanford.edu/class/ee364a/)
 
 ### 7. Machine Learning, Andrew Ng
 * [Machine Learning](https://www.coursera.org/learn/machine-learning) :heavy_check_mark:
 
 ### 8. CS229
 * [CS229: Machine Learning](https://see.stanford.edu/Course/CS229) :heavy_check_mark:
 
 ### 9. CS231n
 * [CS231n: Convolutional Neural Networks for Visual Recognition](http://cs231n.stanford.edu/) :heavy_check_mark:
 
 ### 10. Deep-Learning-Specialization, Andrew Ng
 * [Neural Networks and Deep Learning](https://www.coursera.org/learn/neural-networks-deep-learning?specialization=deep-learning) :heavy_check_mark:
 * [Improving Deep Neural Networks: Hyperparameter tuning, Regularization and Optimization](https://www.coursera.org/learn/deep-neural-network?specialization=deep-learning) :heavy_check_mark:
 * [Structuring Machine Learning Projects](https://www.coursera.org/learn/machine-learning-projects?specialization=deep-learning) :heavy_check_mark:
 * [Convolutional Neural Networks](https://www.coursera.org/learn/convolutional-neural-networks?specialization=deep-learning) :heavy_check_mark:
 * [Sequence Models](https://www.coursera.org/learn/nlp-sequence-models/home/welcome) :heavy_check_mark:
 
 ### 11. Mathematics for Machine Learning
 * [Linear Algebra](https://www.coursera.org/learn/linear-algebra-machine-learning/home/welcome) :heavy_check_mark:
 * [Multivariate Calculus](https://www.coursera.org/learn/multivariate-calculus-machine-learning/home/welcome) :heavy_check_mark:
 * [PCA](https://www.coursera.org/learn/pca-machine-learning/home/welcome) :heavy_check_mark:
 
 ### 12. Python for Everybody
 * [Getting Started with Python](https://www.coursera.org/learn/python/home/welcome) :heavy_check_mark:
 * [Python Data Structures](https://www.coursera.org/learn/python-data/home/welcome) :heavy_check_mark:
 * [Using Python to Access Web Data](https://www.coursera.org/learn/python-network-data/home/welcome) :heavy_check_mark:
 *
 *
 
 ### 13. CS 188
 * [CS 188: Introduction to Artificial Intelligence, Fall 2018](https://inst.eecs.berkeley.edu/~cs188/fa18/)
 
 ### 14. CS294-112
 * [CS 294-112, Deep Reinforcement Learning](http://rail.eecs.berkeley.edu/deeprlcourse/)
 
 ### 15. Machine Learning (2017,Spring), Hung-yi Lee
 * [Machine Learning (2017,Spring)](http://speech.ee.ntu.edu.tw/~tlkagk/courses_ML17.html)
 
 ### 16. Machine Learning and having it deep and structured (2018,Spring), Hung-yi Lee
 * [Machine Learning and having it deep and structured (2018,Spring)](http://speech.ee.ntu.edu.tw/~tlkagk/courses_MLDS18.html)
 
 ### 17. Python 3 Programming
 * [Python Basics](https://www.coursera.org/learn/python-basics?specialization=python-3-programming) :heavy_check_mark:
 *
 *
 *
 *
 
 ### 18. Machine Learning 专项课程
 * [Machine Learning Foundations: A Case Study Approach](https://www.coursera.org/learn/ml-foundations)  ...
 *
 *
 *
 *
 
 ### 19. Advanced Machine Learning 专项课程
 * [Introduction to Deep Learning](https://www.coursera.org/learn/intro-to-deep-learning?specialization=aml) :heavy_check_mark:
 * [How to Win a Data Science Competition: Learn from Top Kagglers](https://www.coursera.org/learn/competitive-data-science)
 *
 *
 *
 *
 *
 
  ### 20. Machine Learning with TensorFlow on Google Cloud Platform 专项课程
 * [How Google does Machine Learning](https://www.coursera.org/learn/google-machine-learning?specialization=machine-learning-tensorflow-gcp)
 *
 *
 *
 *

### 21. Applied Data Science with Python 专项课程
 * [Introduction to Data Science in Python](https://www.coursera.org/learn/python-data-analysis/home/welcome) :heavy_check_mark:
 *
 *
 *
 *
 *
 *


### 22. 数字信号处理
 * [数字信号处理](https://www.coursera.org/learn/dsp) :heavy_check_mark:
 
### 23. CS294-158 Deep Unsupervised Learning Spring 2019
 * [CS294-158](https://sites.google.com/view/berkeley-cs294-158-sp19/home)
 
 
### 24. Learn English: Intermediate Grammar
 * [Perfect Tenses and Modals](https://www.coursera.org/learn/perfect-tenses-modals/home/welcome) :heavy_check_mark:
 *
 *
 *
 
 
### 25. Machine Learning with Python
 * [Machine Learning with Python](https://www.coursera.org/learn/machine-learning-with-python) 
 
### 26. Machine Learning
 * [Machine Learning](https://www.edx.org/course/machine-learning)
 
### 27. Image and Video Processing: From Mars to Hollywood with a Stop at the Hospital
 * [Image and Video Processing: From Mars to Hollywood with a Stop at the Hospital](https://www.coursera.org/learn/image-processing/home/welcome)  :heavy_check_mark:
 
### 28. Introduction to TensorFlow for Artificial Intelligence, Machine Learning, and Deep Learning
* [Introduction to TensorFlow for Artificial Intelligence, Machine Learning, and Deep Learning](https://www.coursera.org/learn/introduction-tensorflow/home/welcome)  :heavy_check_mark:


### 29. AI For Everyone :heavy_check_mark:
 * [AI For Everyone](https://www.coursera.org/learn/ai-for-everyone/home/welcome) 
 
### 30. Julia Scientific Programming
* [Julia Scientific Programming](https://www.coursera.org/learn/julia-programming/home/welcome)
